#ifndef COMMON_RANGE_H
#define COMMON_RANGE_H

namespace common {

template<typename Iter>
struct Range {

    template<typename T>
    Range(T &container)
        : _begin(container.begin())
        , _end(container.end())
    {}

    Range(Iter begin, Iter end)
        : _begin(begin)
        , _end(end)
    {}

    Iter begin() const {
        return _begin;
    }

    Iter end() const {
        return _end;
    }

private:
    Iter _begin;
    Iter _end;
};

template<typename T>
Range(T &container) -> Range<decltype(container.begin())>;

} // namespace common

#endif
