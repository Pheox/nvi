#ifndef COMMON_SYSTEM_ERROR_H
#define COMMON_SYSTEM_ERROR_H

#include <system_error>
#include <errno.h>

namespace common {

struct SystemError : std::system_error {
    SystemError(const char *what)
        : std::system_error(errno, std::generic_category(), what)
    {}
};

struct ConnectionError : SystemError {
    using SystemError::SystemError;
};

} // namespace common

#endif
