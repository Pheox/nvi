#ifndef COMMON_RESIZEABLE_BUFFER_H
#define COMMON_RESIZEABLE_BUFFER_H

#include <string>
#include <type_traits>
#include <vector>

namespace common {

namespace traits {

namespace detail {

template<typename T>
constexpr static bool isChar = std::is_same_v<T, char>
                            || std::is_same_v<T, signed char>
                            || std::is_same_v<T, unsigned char>
                            || std::is_same_v<T, std::byte>;

template<typename T, typename = void>
struct HasSize : std::false_type {};

template<typename T>
struct HasSize<T, std::enable_if_t<std::is_same_v<std::size_t, decltype(std::declval<const T>().size())>>> : std::true_type {};

template<typename T, typename = void>
struct HasData : std::false_type {};

template<typename T>
struct HasData<T, std::enable_if_t<isChar<std::remove_const_t<std::remove_pointer_t<decltype(std::declval<T>().data())>>>>> : std::true_type {};

template<typename T, typename = void>
struct HasResize : std::false_type {};

template<typename T>
struct HasResize<T, decltype(std::declval<T>().resize(std::size_t()))> : std::true_type {};

template<typename T, typename = void>
struct HasCapacity : std::false_type {};

template<typename T>
struct HasCapacity<T, std::enable_if_t<std::is_same_v<std::size_t, decltype(std::declval<const T>().capacity())>>> : std::true_type {};

} // namespace common::traits::detail;

template<typename T>
constexpr static bool isChar = detail::isChar<T>;

template<typename T>
constexpr static bool isAcceptableContainer = detail::HasData<T>::value
                                           && detail::HasSize<T>::value;

template<typename T>
constexpr static bool isResizeableContainer = isAcceptableContainer<T>
                                           && detail::HasCapacity<T>::value
                                           && detail::HasResize<T>::value;

} // namespace common::traits

template<typename Container>
struct ResizeableBuffer {

    static_assert(traits::isResizeableContainer<Container>, "The container must implement capacity and resize methods");

    ResizeableBuffer(Container &container)
        : _container(container)
    {
        _container.resize(_container.capacity());
    }

    template<typename Size>
    Size shrink(Size size) && {
        _container.resize(size);
        return size;
    }

private:
    Container &_container;
};

} // namespace common

#endif
