#ifndef DOC_ERROR_H
#define DOC_ERROR_H

#include <stdexcept>

namespace doc {

/**
 * @brief Exception class to report errors during document manipulation.
 * 
 */
struct Error : std::runtime_error {
    using std::runtime_error::runtime_error;
};

} // namespace doc

#endif
