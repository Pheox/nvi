#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>

#include "../common/net.h"
#include "../document/cli.h"


int main(int argc, char **argv) {
    log::sink.set<std::ofstream>("test.log", std::ios::app | std::ios::out);
    doc::CLI().run();
    return 0;
}

